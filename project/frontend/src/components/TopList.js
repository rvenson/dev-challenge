import React from 'react'
import LinkList from './LinkList'

export default class TopList extends React.Component {

  constructor(props){
    super(props)
  }

  render(){

    this.state = { linkList : this.props.linkList}

    let topListFrame
    if(this.state.linkList.length > 0) {
      topListFrame = <div><h2>Top 5</h2>
      <div class='d-flex justify-content-around'>
        <div class='link-list'>
          <LinkList linkList={this.state.linkList} serverUrl={this.props.serverUrl}/>
        </div>
      </div>
    </div>
    } else {
      topListFrame = <h1>We don't have any links yet!</h1>
    }

    return(
      <div class='container'>
        {topListFrame}
      </div>
    );
  }
}
