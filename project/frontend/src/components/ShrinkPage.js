import React from 'react'
import ShrinkForm from './ShrinkForm'

export default class ShrinkPage extends React.Component  {

  constructor(props){
    super(props)
  }

  handleSubmit = (event, value) => {
    this.props.onSubmit(event, value)
  }

  render(){
    let interlinkImg = require('../resources/img/interlink-logo-white.png')
    let backgroundImg = require('../resources/img/background.jpg')
    return(
      <div class='text-center'>
        <div class='row d-flex justify-content-center'>
          <div class='col-sm'>
            <img src={interlinkImg} class='interlink-logo' />
          </div>
          <div class='col-8 shrink-page'>
            <h1>Shrink your link!</h1>
            A long URL is always a problem. It's hard to remember and share.
            <ShrinkForm linkList={this.props.linkList} onSubmit={this.handleSubmit}/>
          </div>
          <div class='col-sm'>
          </div>
        </div>
      </div>
    );
  }
}
