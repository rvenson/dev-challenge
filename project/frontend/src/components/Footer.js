import React from 'react'
import heartImg from '../resources/img/heart-emoji.png'
import twitterImg from '../resources/img/twitter-logo.png'

export default class Footer extends React.Component {
  render(){
    return(
      <div class='container'>
        <div class='row d-flex justify-content-between'>
          <div class='p2'>Made with <img src={heartImg} class="emoji img-fluid"/> by Interlink</div>
          <div class='p2'><img src={twitterImg} class="emoji img-fluid"/></div>
        </div>
      </div>
    );
  }
}
