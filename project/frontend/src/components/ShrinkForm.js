import React from 'react'

export default class ShrinkForm extends React.Component  {

  constructor(props){
    super(props)
    this.state = { value: ''}
  }

  handleChange = (event) => {
    this.setState({value: event.target.value})
  }

  handleSubmit = (event) => {
    this.props.onSubmit(event, this.state.value)
    event.preventDefault()
  }

  render(){
    return(
      <form onSubmit={this.handleSubmit}>
        <div class='input-group mb-3 shrink-form'>
          <input onChange={this.handleChange} value={this.state.value} type='text' class='form-control' placeholder='Paste the link to shrink it' />
          <div class='input-group-append'>
            <button class='interlink-btn btn btn-outline'>Shrink</button>
          </div>
        </div>
      </form>
    );
  }
}
