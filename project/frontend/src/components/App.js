import React from 'react'
import ShrinkPage from './ShrinkPage'
import TopList from './TopList'
import Footer from './Footer'
import Axios from 'axios'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SERVER_URL = 'http://localhost:5000/'

export default class App extends React.Component {

  constructor(props){
    super(props)

    const toastOptions = {
      position: "top-right",
      autoClose: false,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false
    }

    this.state = {
      linkList: [],
      serverUrl: SERVER_URL
    }
  }

  componentDidMount(){
    this.updateLinkList()
  }

  updateLinkList(){
    Axios.get(SERVER_URL + 'links/top').then(res => {
      this.setState({ linkList: res.data.linkList})
    }).catch(err => {
      toast.error('The shortener gnomes are missing! =(', this.toastOptions);
    })
  }

  handleSubmit = (event, value) => {

    let lastToast = toast.info('Shortening link... wait for this...', this.toastOptions);

    Axios.post(SERVER_URL + 'links/', {link: value}).then(res => {
      toast.update(lastToast, {
        type: toast.TYPE.SUCCESS,
        render: "Your link was shortened! " + value + " now is " + SERVER_URL + res.data.link,
      });

      this.updateLinkList()

    }).catch(err => {
      //toast.error('', this.toastOptions);
      toast.update(lastToast, {
        type: toast.TYPE.ERROR,
        render: "Your link can\'t be shortened! (" + value + ")",
        draggable: true,
        closeOnClick: true,
        autoClose: 5000
      });
    })
  }

  render(){
    return(
      <div>
        <ToastContainer
          position="top-right"
          autoClose={false}
          newestOnTop
          closeOnClick={false}
          rtl={false}
          pauseOnVisibilityChange
          draggable={false}
          />
        <header>
          <ShrinkPage linklist={this.state.linkList} onSubmit={this.handleSubmit}/>
        </header>
        <main class='text-center d-flex justify-content-center'>
          <TopList linkList={this.state.linkList} serverUrl={this.state.serverUrl}/>
        </main>
        <footer>
          <Footer />
        </footer>
      </div>
    )}
}
